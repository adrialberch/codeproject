package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestApp2 {

	@Test
	public void testingCrunchifyAddition() {
		assertEquals("Here is test for Addition Result: ", 30, addition(27, 3));
	}
 
	@Test
	public void testingHelloWorld() {
		assertEquals("Here is test for Hello World String: ", "Hello + World", helloWorld());
	}
 
	public int addition(int x, int y) {
		return x + y;
	}
 
	public String helloWorld() {
		String helloWorld = "Hello +" + " World";
		return helloWorld;
	}
	
}
